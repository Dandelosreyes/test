<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/','login');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('migrations','MigrationsController')->middleware('auth')->except(['edit']);
Route::get('search','MigrationsController@search')->middleware('auth')->name('migrations.search');
Route::get('activitylog','HomeController@logs')->middleware('auth')->name('actlogs');
Route::post('search','MigrationsController@search_action')->name('migrations.search.action')->middleware('auth');
Route::get('migrations/sorting/completed','MigrationsController@completed')->middleware('auth')->name('migrations.completed');
Route::get('migrations/sorting/scheduled','MigrationsController@scheduled')->middleware('auth')->name('migrations.scheduled');
Route::get('migrations/sorting/pending','MigrationsController@pending')->middleware('auth')->name('migrations.pending');
Route::get('migrations/sorting/cancelled','MigrationsController@cancelled')->middleware('auth')->name('migrations.cancelled');
Route::get('log','HomeController@log')->name('log');
Route::get('storage/{filename}',function($filename){
    // $path = storage_path($filename);
    $path = str_replace('public/','replace',$filename);
    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
})->name('getFile');
// Route::get('email', function(){
// 	Mail::raw('Sending emails with Mailgun and Laravel is easy!', function($message)
// 	{
// 		$message->to('batrider69@gmail.com');
// 	});
// });
Route::resource('users','UserController')->except(['destroy']);


Route::get('reports/completed','ExcelController@completed')->name('reports.completed');
Route::get('reports/scheduled','ExcelController@scheduled')->name('reports.scheduled');
Route::get('reports/pending','ExcelController@pending')->name('reports.pending');
Route::get('reports/cancelled','ExcelController@cancelled')->name('reports.cancelled');
Route::get('reports/all','ExcelController@all')->name('reports.all');