<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\ServiceRequestNumber;
use Auth;
class NewMigrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(Auth::check())
        {
            return true;   
        }
        else
        {
            return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'service_request_number' => ['required',new ServiceRequestNumber],
            'company_name' => 'required',
            'date_of_migration' => 'required|date_format:m/d/Y',
            'old_van' => 'required|different:new_van',
            'new_van' => 'required',
            'type_id' => 'required|integer',
            'status_id' => 'required|integer',
            'edi_id' => 'required'
        ];
    }
}
