<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Carbon\Carbon;
use Excel;
use App\Models\Transportation;
class ExcelController extends Controller
{
    public function completed()
    {
    	$data = Transportation::where('status_id',1)->get();
			Excel::create('Completed Migrations'.Carbon::now()->toDateString(), function($excel) use($data) {
			    $excel->sheet('Sheet 1', function($sheet) use($data) {
			        $sheet->fromArray($data);
			    });
		})->export('xls');
    }
    public function scheduled()
    {
    	$data = Transportation::where('status_id',2)->get();
			Excel::create('Scheduled Migrations'.Carbon::now()->toDateString(), function($excel) use($data) {
			    $excel->sheet('Sheet 1', function($sheet) use($data) {
			        $sheet->fromArray($data);
			    });
		})->export('xls');
    }
    public function pending()
    {
    	$data = Transportation::where('status_id',3)->get();
			Excel::create('Pending Migrations'.Carbon::now()->toDateString(), function($excel) use($data) {
			    $excel->sheet('Sheet 1', function($sheet) use($data) {
			        $sheet->fromArray($data);
			    });
		})->export('xls');
    }
    public function cancelled()
    {
    	$data = Transportation::where('status_id',4)->get();
			Excel::create('Cancelled Migrations '.Carbon::now()->toDateString(), function($excel) use($data) {
			    $excel->sheet('Sheet 1', function($sheet) use($data) {
			        $sheet->fromArray($data);
			    });
		})->export('xls');
    }
    public function all()
    {
    	$data = Transportation::all();
			Excel::create('Cancelled Migrations '.Carbon::now()->toDateString(), function($excel) use($data) {
			    $excel->sheet('Sheet 1', function($sheet) use($data) {
			        $sheet->fromArray($data);
			    });
		})->export('xls');
    }

}
