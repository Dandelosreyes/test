<?php

namespace App\Http\Controllers;

use App\Models\Transportation;
use Illuminate\Http\Request;
use App\Models\Type;
use App\Models\Status;
use Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\NewMigrationRequest;
use App\Models\ActivityLog;
class MigrationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Transportations = Transportation::all();
        return view('migrations.index')->with('data',$Transportations);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
                // dd(Storage::files('photos'));

        return view('operations.create')->with(['statuses' => Status::all() , 'types' => Type::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewMigrationRequest $request)
    {
        $migration = new Transportation;
        $migration->service_request = $request->service_request_number;
        $migration->company_name = $request->company_name;
        $migration->date_of_migration = \Carbon\Carbon::parse( $request->date_of_migration);
        $migration->old_van = $request->old_van;
        $migration->new_van = $request->new_van;
        $migration->type_id = $request->type_id;
        $migration->status_id = $request->status_id;
        $migration->edi_id = $request->edi_id;
        $migration->attached_file = Storage::disk('local')->putFile('photos',$request->file('attached_file'));
        $migration->save();
        ActivityLog::create(['user_id' => Auth::user()->id , 'action' => 'Added a migration with the id of '.$migration->id]);
        return back()->with('status','Migration added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transportation  $transportation
     * @return \Illuminate\Http\Response
     */
    public function show(Transportation $transportation, $id)
    {
        $data  = Transportation::find($id);
        return view('operations.show')->with(['data' => $data , 'statuses' => Status::all() , 'types' => Type::all() ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Transportation  $transportation
     * @return \Illuminate\Http\Response
     */
    public function edit(Transportation $transportation, $id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Transportation  $transportation
     * @return \Illuminate\Http\Response
     */
    public function update(NewMigrationRequest $request, Transportation $transportation,$id)
    {
        $migration = Transportation::find($id);
        $migration->service_request = $request->service_request_number;
        $migration->company_name = $request->company_name;
        $migration->date_of_migration = \Carbon\Carbon::parse( $request->date_of_migration);
        $migration->old_van = $request->old_van;
        $migration->new_van = $request->new_van;
        $migration->type_id = $request->type_id;
        $migration->status_id = $request->status_id;
        $migration->edi_id = $request->edi_id;
        $migration->attached_file =  Storage::disk('local')->putFile('public',$request->file('attached_file'));
        $migration->save();
        ActivityLog::create([ 'user_id' => Auth::user()->id ,'action' => 'Updated a migration with the id of '.$id]);
        return back()->with('status','Migration updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transportation  $transportation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transportation $transportation,$id)
    {
        Transportation::find($id)->delete();
        ActivityLog::create(['user_id' => Auth::user()->id, 'action' => 'Removed a migration with the id of '.$id]);
        return redirect()->route('migrations.index')->with('status','Migration removed.');
    }

    public function search()
    {
        return view('operations.search');
    }
    public function search_action(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'srn' => 'required',
            'cname' => 'required',
            'edi_id' => 'required'
        ]);
        $result = Transportation::search($request->id,$request->srn,$request->cname,$request->edi_id);
        // dd($result);
        if($result->isEmpty())
        {
            return back()->with('status','No result found')->withInput();
        }
        else{
            return redirect()->route('migrations.show',['id' => $request->id ]);
        }
    }
    // Sortings
    public function completed()
    {
        return view('migrations.completed')
                    ->with('data',Transportation::where('status_id',1)->get());
    }
    public function scheduled()
    {
        return view('migrations.scheduled')
                    ->with('data',Transportation::where('status_id',2)->get());
    }
    public function pending()
    {
        return view('migrations.pending')
                    ->with('data',Transportation::where('status_id',3)->get());
    }
    public function cancelled()
    {
        return view('migrations.cancelled')
                    ->with('data',Transportation::where('status_id',4)->get());
    }
}
