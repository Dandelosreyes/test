<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transportation;
use Carbon\Carbon;
use App\Models\ActivityLog;
use Mail;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home')->with('migrations',Transportation::whereDate('date_of_migration','=',Carbon::today()->toDateString())->get());
    }
    public function log()
    {
        return view('log')->with('logs',ActivityLog::all());
    }
    public function email()
    {

    }
}
