<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Transportation extends Model
{
    public function type()
    {
    	return $this->belongsTo('App\Models\Type');
    }
    public function status()
    {
    	return $this->belongsTo('App\Models\Status','status_id');
    }
    public function setDateOfMigration($value)
    {
    	$this->attributes['date_of_migration'] = date('m-d-y',strtotime($value));
    }
    public function scopeSearch($query,$id,$srn,$cname,$edi_id)
    {
        return $query->where('id','=',$id)
            ->where('service_request','=',$srn)
            ->where('company_name','like','%'.$cname.'%')
            ->where('edi_id','=',$edi_id)
            ->get();
    }
}
