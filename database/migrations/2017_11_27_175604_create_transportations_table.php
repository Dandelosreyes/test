<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransportationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transportations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('service_request');
            $table->string('company_name');
            $table->string('edi_id');
            $table->date('date_of_migration');
            $table->string('old_van');
            $table->string('new_van');
            $table->unsignedInteger('type_id');
            $table->string('attached_file')->nullable();
            $table->foreign('type_id')->references('id')->on('types');
            $table->unsignedInteger('status_id');
            $table->foreign('status_id')->references('id')->on('statuses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transportations');
    }
}
