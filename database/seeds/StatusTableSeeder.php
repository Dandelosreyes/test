<?php

use Illuminate\Database\Seeder;
use App\Models\Status;
class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Status::create([
        	'status' => 'Completed'
        ]);
        Status::create([
        	'status' => 'Scheduled'
        ]);
        Status::create([
        	'status' => 'Pending'
        ]);
        Status::create([
        	'status' => 'Cancelled'
        ]);

    }
}
