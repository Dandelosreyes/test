<?php

use Illuminate\Database\Seeder;
use App\Models\Transportation;
use Carbon\Carbon;
class TransportationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Transportation::create([
        	'service_request' => '1-3078217736',
        	'company_name' => 'KOHLER INC.',
        	'EDI_ID' => '12:435546456',
        	'date_of_migration' => Carbon::now()->addDay(1),
        	'old_van' => 'GXS',
        	'new_van' => 'Sterling',
        	'type_id' => 5,
        	'status_id' => 4
        ]);
    }
}
