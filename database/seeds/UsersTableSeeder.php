<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	'name' => 'Olive Alelojo',
        	'email' => 'oalelojo@opentext.com',
        	'password' => '123'
        ]);
        User::create([
        	'name' => 'Doria',
        	'email' => 'cdoria@opentext.com',
        	'password' => '123'
        ]);
        User::create([
        	'name' => 'Erika Valdez',
        	'email' => 'evaldez@opentext.com',
        	'password' => '123'
        ]);
        User::create([
        	'name' => 'Capuyan',
        	'email' => 'mcapuyan@opentext.com',
        	'password' => '123'
        ]);
    }
}
