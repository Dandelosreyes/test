<?php

use Illuminate\Database\Seeder;
use App\Models\Type;
class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Type::create([
        	'type' => 'GXS TGMS to Interconnect'
        ]);
        Type::create([
        	'type' => 'IE to Interconnect'
        ]);
        Type::create([
        	'type' => 'INOVIS to Interconnect'
        ]);
        Type::create([
        	'type' => 'EASYLINK GMS to Interconnect'
        ]);
        Type::create([
        	'type' => 'EASYLINK ICC to Interconnect'
        ]);
        Type::create([
        	'type' => 'COVISINT to Interconnect'
        ]);
        Type::create([
        	'type' => 'Internal Migration to GXS TGMS'
        ]);
        Type::create([
        	'type' => 'Internal Migration to IE'
        ]);
        Type::create([
        	'type' => 'Internal Migration to INOVIS'
        ]);
        Type::create([
        	'type' => 'Internal Migration to EASYLINK GMS'
        ]);
        Type::create([
        	'type' => 'Internal Migration to EASYLINK ICC'
        ]);
        Type::create([
        	'type' => 'Internal Migration to COVISINT'
        ]);
        Type::create([
        	'type' => 'Interconnect to GXS TGMS'
        ]);
        Type::create([
        	'type' => 'Interconnect to IE'
        ]);
        Type::create([
        	'type' => 'Interconnect to INOVIS'
        ]);
        Type::create([
        	'type' => 'Interconnect to EASYLINK GMS'
        ]);
        Type::create([
        	'type' => 'Interconnect to EASYLINK ICC'
        ]);
        Type::create([
        	'type' => 'Interconnect to COVISINT'
        ]);

    }
}
