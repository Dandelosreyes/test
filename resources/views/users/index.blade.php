@extends('layouts.app')
@section('content')
<div class="container">
	<div class="card card-header">
		<div class="row">
			<div class="col-10">Users</div>
			<div class="col-2">
				<a href="{{ route('users.create') }}" class="btn btn-info float-right btn-sm">Create User</a>
			</div>
		</div>
	</div>
	<div class="card card-body">
		<table id="UsersTable" class="table table-bordered">
			<thead>
				<th>ID</th>
				<th>Name</th>
				<th>Email</th>
				<th>Date Created</th>
			</thead>
			<tbody>
				@foreach($users as $user)
				<tr>
					<td>{{ $user->id}}</td>
					<td>{{ $user->name }}</td>
					<td>{{ $user->email }}</td>
					<td>{{ $user->created_at->format('d-m-Y') }}</td>
				</tr>
				@endforeach
				
			</tbody>
		</table>
	</div>
	<div class="card card-footer">
		Updated as of {{ Carbon\Carbon::now()->toDateTimeString() }}
	</div>
</div>
@endsection