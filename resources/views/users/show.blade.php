@extends('layouts.app')
@section('content')
<div class="container">
	<div class="card card-info">
		<div class="card-header">My Profile</div>
		<div class="card-body">
			<div class="row">
				<div class="col-8">
					<p>
						<h5 class="inline">Name:</h5> {{ $user->name }}
					</p>
					<p>
						<h5 class="inline">Email:</h5> {{ $user->email }}
					</p>
				</div>
				<div class="col-4">
					<img src="{{ asset('uploads/acct.png') }}	">
				</div>
			</div>
		</div>
		<div class="card-footer">
			Last Update {{ $user->updated_at }}
			<a href='{{ route('users.edit',$user->id)}}' class="btn btn-info btn-sm float-right">Edit</a>
		</div>
	</div>
</div>
@endsection
@push('styles')
<style type="text/css">
	.inline{
		display: inline-block;
	}
</style>
@endpush
