@extends('layouts.app')
@section('content')
<div class="container">
	<div class="card card-header">
		<div class="row">
			<div class="col-10">
				Add User
			</div>
			<div class="col-2">
				<a href="{{ route('users.index') }}" class="btn btn-sm btn-info float-right">Back</a>
			</div>
		</div>
	</div>
	<div class="card card-body">
		@include('shared.alerts')
		<form method="POST" action="{{ route('users.store') }}">
			{{ csrf_field() }}
			<div class="form row">
				<div class="col-6">
					<label for="name">Name</label>
					<input class ="form-control" type="text" name="name" id="name" value="{{ old('name') }}">
				</div>
				<div class="col-6">
					<label for="email">Email</label>
					<input class="form-control" type="text" name="email" id="name" value=" {{ old('email') }}">
				</div>
			</div>
			<div class="form row">
				<div class="col-6">
					<label for="password">Password</label>
					<input class ="form-control" type="password" name="password" id="password">
				</div>
				<div class="col-6">
					<label for="password_confirmation">Confirm Password:</label>
					<input class="form-control" type="password" name="password_confirmation" id="password_confirmation">
				</div>
			</div>
			<div class="form row mt-3">
				<div class="col-12">
					<button class="btn btn-info">Save</button>
				</div>
			</div>
		</form>
	</div>
	<div class="card card-footer">
		Date Today {{ Carbon\Carbon::now()->toDateString() }}
	</div>
</div>
@endsection