@extends('layouts.app')
@section('content')
<div class="container">
	<div class="card card-dark">
		<div class="card-header">
			Updating My Profile
			<a href="{{ route('users.show',$user->id)}}" class="btn btn-sm btn-info float-right">Back</a>
		</div>
		<div class="card-body">
			@include('shared.alerts')
		<form method="POST" action="{{ route('users.update',['id' => $user->id]) }}">
			{{ csrf_field() }}
			{{ method_field('PUT')}}
			<div class="form row">
				<div class="col-6">
					<label for="name">Name</label>
					<input class ="form-control" type="text" name="name" id="name" value="{{ old('name') ?: $user->name }}">
				</div>
				<div class="col-6">
					<label for="email">Email</label>
					<input class="form-control" type="text" name="email" id="name" value=" {{ old('email') ?: $user->email }}">
				</div>
			</div>
			<div class="form row">
				<div class="col-6">
					<label for="password">Password</label>
					<input class ="form-control" type="password" name="password" id="password">
				</div>
				<div class="col-6">
					<label for="password_confirmation">Confirm Password:</label>
					<input class="form-control" type="password" name="password_confirmation" id="password_confirmation">
				</div>
			</div>
			<div class="form row mt-3">
				<div class="col-12">
					<button class="btn btn-info">Save</button>
				</div>
			</div>
		</form>
		</div>
	</div>
</div>
@endsection