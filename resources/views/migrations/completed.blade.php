@extends('layouts.app')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12 col-lg-2 col-md-2">
			<h4>Sorting</h4>
			<div class="btn-group-vertical" role="group">
				<a href="{{ route('migrations.index') }}" class="btn btn-outline-secondary text-dark  ">View Migrations(All)</a>
				<a href="{{ route('migrations.completed') }}" class="btn btn-secondary text-light">Completed Migrations</a>
				<a href="{{ route('migrations.scheduled') }}" class="btn btn-outline-secondary text-dark">Scheduled Migrations</a>
				<a href="{{ route('migrations.pending') }}" class="btn btn-outline-secondary text-dark">Pending Migrations</a>
				<a href="{{ route('migrations.cancelled') }}" class="btn btn-outline-secondary text-dark">Cancelled Migrations</a>
			</div>
			<h4>Operations</h4>
			<div class="btn-group-vertical" role="group">
				<a class="btn btn-outline-secondary text-dark" href="{{ route('migrations.create') }}">Add Migrations</a>
				<a class="btn btn-outline-secondary text-dark" href="{{ route('migrations.search') }}">Search Migrations</a>
			</div>
		</div>
		<div class="col-sm-12 col-lg-10 col-md-10">
			<h4>Migrations</h4>
			@if (session('status'))
			    <div class="alert alert-success">
			        {{ session('status') }}
			    </div>
			@endif
			<div class="card card-default">
				<div class="card-header">
					Completed Migrations
					<button type="button" class="btn btn-outline-info float-right btn-sm" data-toggle="modal" data-target="#modalHelp">
					  STATUSES
					</button>
				</div>
				<div class="card-body">
					<table class="table table-bordered table-responsive" id="myTable">
						<thead class="thead-light">
							<th>ID</th>
							<th>Service Request #</th>
							<th>Company Name</th>
							<th>EDI ID</th>
							<th>Date of Migration</th>
							<th>Old Network</th>
							<th>New Network</th>
							<th>Type</th>
							<th>Status</th>
							<th>File</th>
						</thead>
						<tbody>
							@foreach($data as $d)
							<tr 
								class="
									@if($d->status_id == 1) 
									table-success
									@elseif($d->status_id == 2)
									table-info
									@elseif($d->status_id == 3)
									table-primary
									@else
									table-danger
									@endif
								">
								<td>{{ $d->id }}</td>
								<td>{{ $d->service_request }}</td>
								<td>{{ $d->company_name }}</td>
								<td>{{ $d->edi_id }}</td>
								<td>{{ $d->date_of_migration }}</td>
								<td>{{ $d->old_van }}</td>
								<td>{{ $d->new_van }}</td>
								<td>{{ $d->type->type }}</td>
								<td>
									{{ $d->status->status }}</td>
								</td>
								<td>
									@if($d->attached_file) 
									<a href="{{ route('getFile',['filename' => str_replace('public/', '', $d->attached_file)]) }}">Click Here</a>
									@else
									No File.
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="card-footer">
					Updated as of {{ Carbon\Carbon::now()->toDayDateTimeString() }}
					<a href="{{ route('reports.completed') }}" class="btn btn-info btn-sm float-right">Export to Excel</a>

				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div class="modal fade" id="modalHelp" tabindex="-1" role="dialog" aria-labelledby="modalHelp" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalHelp">Statuses</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>
            &raquo; <span class="modalText text-white bg-warning">Pending</span> status means the migration is already pending.
        </p>
        <p>
            &raquo; <span class="modalText text-white bg-success">Completed</span> status means the migration is completed .
        </p>
        <p>
            &raquo; <span class="modalText text-white bg-info">Scheduled</span> status means that the migration is going to happen on the said date.
        </p>
        <p>
            &raquo; <span class="modalText text-white bg-danger">Cancelled</span> status means 
            there's something wrong with the migration or the client request for the migration to be cancelled.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection
@push('scripts')
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script>
$('#myTable').DataTable();
</script>
@endpush
@push('styles')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
@endpush