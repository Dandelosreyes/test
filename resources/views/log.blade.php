@extends('layouts.app')
@section('content')
<div class="container">
	<div class="card card-outline-dark">
		<div class="card-header">Activity Log</div>
		<div class="card-body">
			<table class="table table-bordered table-hover" id="myTable">
				<thead class="thead-dark">
					<th>ID</th>
					<th>Email</th>
					<th>Action</th>
					<th>Date of Action</th>
				</thead>
				<tbody>
					@foreach($logs as $log)
					<tr>
						<td>{{ $log->id }}</td>
						<td>{{ $log->user->email }}</td>
						<td>{{ $log->action }}</td>
						<td>{{ $log->created_at->format('m-d-Y')}}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="card-footer">Updated as of {{ \Carbon\Carbon::now()->toDateString() }}</div>
	</div>
</div>
@endsection
@push('scripts')
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script>
$('#myTable').DataTable();
</script>
@endpush
@push('styles')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
@endpush