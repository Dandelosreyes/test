@extends('layouts.app')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12 col-lg-2 col-md-2">
			<h4>Sorting</h4>
			<div class="btn-group-vertical" role="group">
				<a href="{{ route('migrations.index') }}" class="btn btn-outline-secondary text-dark">View Migrations(All)</a>
				<a href="{{ route('migrations.completed') }}" class="btn btn-outline-secondary text-dark">Completed Migrations</a>
				<a href="{{ route('migrations.scheduled') }}"class="btn btn-outline-secondary text-dark">Scheduled Migrations</a>
				<a href="{{ route('migrations.pending') }}" class="btn btn-outline-secondary text-dark">Pending Migrations</a>
				<a href="{{ route('migrations.cancelled') }}" class="btn btn-outline-secondary text-dark">Cancelled Migrations</a>
			</div>
			<h4>Operations</h4>
			<div class="btn-group-vertical" role="group">
				<a class="btn btn-outline-dark  text-dark" href="{{ route('migrations.create') }}">Add Migrations</a>
				<a class="btn btn-secondary text-light" href="{{ route('migrations.search') }}">Search Migrations</a>
			</div>
		</div>
		<div class="col-sm-12 col-lg-10 col-md-10">
			<h4>Migrations</h4>
			<div class="card card-default">
				<div class="card-header">
					Edit Migration
				</div>
				<div class="card-body">
					@if ($errors->any())
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
					@endif
					@if (session('status'))
					    <div class="alert alert-success">
					        {{ session('status') }}
					    </div>
					@endif

					<form method="POST" action="{{ route('migrations.update',['id' => $data->id]) }}" enctype="multipart/form-data">
						<div class="form-row">
							{{ method_field('PATCH') }}
							{{ csrf_field() }}
					    	<div class="form-group col-md-6">
				    			<label for="service_request_number">Service Request Number</label>
						      	<input type="text" class="form-control" id="service_request_number" name="service_request_number" placeholder="x-xxxxxxxxxx" value="{{ old('service_request_number') ?: $data->service_request }}">
					        </div>
						    <div class="form-group col-md-6">
					    			<label for="company_name">Company name</label>
						      	<input type="text" class="form-control" id="company_name" name="company_name" placeholder="Kohler Inc." value="{{ old('company_name') ?: $data->company_name }}">
						    </div>
					    </div>
					     <div class="form row">
						   	<div class="form-group col-md-6">
				    			<label for="edi_id">EDI ID</label>
						      	<input type="text" class="form-control" id="edi_id" name="edi_id" placeholder="12:123123123" value="{{ old('edi_id') ?: $data->edi_id }}">
					        </div>
					        <div class="form-group col-md-6">
				    			<label for="date_of_migration">Date of Migration</label>
						      	<input type="text" class="form-control" id="date_of_migration" name="date_of_migration" placeholder="mm/dd/yyyy" value="{{ old('date_of_migration') ?: Carbon\Carbon::parse($data->date_of_migration)->format('m/d/Y') }}">
					        </div>
					    </div>
					    <div class="form row">
					        <div class="form-group col-md-6">
				    			<label for="old_van">Old Network</label>
					      		<input type="text" class="form-control" id="old_van" name="old_van" placeholder="GXS" value ="{{ old('old_van') ?: $data->old_van }}">
					        </div>
					        <div class="form-group col-md-6">
				    			<label for="new_van">New Network</label>
					      		<input type="text" class="form-control" id="new_van" name="new_van" placeholder="Sterling" value="{{ old('new_van') ?:  $data->new_van }}">
				      	    </div>
					    </div>
					    <div class="form row">
						  	<div class="form-group col-md-6">
				    			<label for="status_id">Status</label>
						      	<select class="form-control" name="status_id"> 
						      		@foreach($statuses as $status)
						      		<option value="{{ $status->id }}" @if($data->status_id == $status->id ) selected @endif>{{ $status->status }}</option>
						      		@endforeach
						      	</select>
				        	</div>
					        <div class="form-group col-md-6">
				    			<label for="type_id">Type</label>
						      	<select class="form-control" name="type_id"> 
											@foreach($types as $type)
						      		<option value="{{ $type->id }}" @if($data->type_id == $type->id ) selected @endif>{{ $type->type }}</option>
						      		@endforeach
						      	</select>
					        </div>
					        <div class="form-group col-md-12">
				    			<label for="attached_file">File Attachment</label>
						      	<input type="file" name='attached_file' id='attached_file' class="form-control">
					        </div>
					    </div>
					    <div class="form row">
						  	<div class="col-12">
						  		<button class="btn btn-outline-primary">Update</button>
						  		<!--<a href="#" class="btn btn-outline-danger float-right" onclick="event.preventDefault();-->
          <!--                                                   document.getElementById('remove-form').submit();">Remove</a>-->
						  	</div>
					    </div>
					</form>
					<form id="remove-form" action="{{ route('migrations.destroy',['id' => $data->id ]) }}" method="POST" style="display: none;">
						{{ method_field('DELETE') }}
                      	{{ csrf_field() }}
                    </form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection