@extends('layouts.app')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12 col-lg-2 col-md-2">
			<h4>Sorting</h4>
			<div class="btn-group-vertical" role="group">
				<a href="{{ route('migrations.index') }}" class="btn btn-outline-secondary text-dark">View Migrations(All)</a>
				<a href="{{ route('migrations.completed') }}" class="btn btn-outline-secondary text-dark">Completed Migrations</a>
				<a href="{{ route('migrations.scheduled') }}"class="btn btn-outline-secondary text-dark">Scheduled Migrations</a>
				<a href="{{ route('migrations.pending') }}" class="btn btn-outline-secondary text-dark">Pending Migrations</a>
				<a href="{{ route('migrations.cancelled') }}" class="btn btn-outline-secondary text-dark">Cancelled Migrations</a>
			</div>
			<h4>Operations</h4>
			<div class="btn-group-vertical" role="group">
				<a class="btn btn-outline-secondary text-dark" href="{{ route('migrations.create') }}">Add Migrations</a>
				<a class="btn btn-secondary text-light">Search Migrations</a>
			</div>
		</div>
		<div class="col-sm-12 col-lg-10 col-md-10">
			<h4>Migrations</h4>
			<div class="card card-default">
				<div class="card-header">
					Search Migration
				</div>
				<div class="card-body">
					@if ($errors->any())
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif
					@if (session('status'))
					    <div class="alert alert-danger">
					        <ul>
					        	<li>{{ session('status') }}</li>
					        </ul>
					    </div>
					@endif
				</div>
				<form method="POST" action="{{ route('migrations.search.action') }}" class="ml-3 mr-3">
					{{ csrf_field() }}
			    	<div class="form-group row ">
					    <div class="col-3">
					   	 	<label for="id" class=" col-form-label">ID</label>
						     <input type="text" class="form-control" id="id" name="id" value="{{ old('id') }}">
					    </div>
					    <div class="col-3">
					   	 	<label for="srn" class=" col-form-label">Service Request Number</label>
						     <input type="text" class="form-control" id="srn" name="srn"  value="{{ old('srn') }}">
					    </div>
					    <div class="col-3">
					   	 	<label for="cname" class=" col-form-label">Company Namef</label>
						     <input type="text" class="form-control" id="cname" name="cname"  value="{{ old('cname') }}">
					    </div>
					    <div class="col-3">
					   	 	<label for="edi_id" class=" col-form-label">EDI ID</label>
						     <input type="text" class="form-control" id="edi_id" name="edi_id"  value="{{ old('edi_id') }}">
					   </div>
				    </div>
				    <button class="btn btn-outline-info mb-4">Search</button>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection