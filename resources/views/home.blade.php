@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 mx-auto">
            <div class="card panel-default">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <h5>Date Today:  {{ Carbon\Carbon::today()->toDateString() }}</h5>
                    @if($migrations->isEmpty())
                    <strong>No Migrations Scheduled for today.</strong>
                    @else
                    <h4>Migrations Scheduled for today</h4>
                    <table class="table table-bordered">
                        <thead>
                            <th>ID</th>
                            <th>Company Name</th>
                            <th>EDI ID</th>
                            <th>Service Request Number</th>
                        </thead>
                        <tbody>
                            @foreach($migrations as $m)
                            <td>{{ $m->id }}</td>
                            <td>{{ $m->company_name}}</td>
                            <td>{{ $m->edi_id }}</td>
                            <td>{{ $m->service_request }}</td>
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
